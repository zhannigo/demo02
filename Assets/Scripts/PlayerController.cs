using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions _playerInputActions;
    public Rigidbody _playerRigidbody;
    [SerializeField] private float _moveSpeed;
    public Vector3 _startPosition;
    public float _maxSpeed = 1f;
    private Vector2 moveDirection;
    private Vector3 angleVelocity;

    void Awake()
    {
        _playerInputActions = new PlayerInputActions();
        _playerRigidbody = GetComponent<Rigidbody>();
        _startPosition = transform.position;
        _playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();
        _playerInputActions.Kart.SpeedUp.performed += context => SpeedUp();
    }

    private void OnEnable()
    {
        _playerInputActions.Enable();
    }

    private void OnDisable()
    {
        _playerInputActions.Disable();
    }

    private void Update()
    {
        moveDirection = _playerInputActions.Kart.Move.ReadValue<Vector2>();
        
        //Set the angular velocity of the Rigidbody (rotating around the Y axis, 100 deg/sec)
        angleVelocity = new Vector3(0, moveDirection.x, 0);
    }

    private void FixedUpdate()
    {
        Move(moveDirection);
    }

    void Move(Vector2 direction)
    {
        Vector3 movement = new Vector3(direction.x * _moveSpeed * Time.fixedDeltaTime, 0, direction.y * _moveSpeed * Time.fixedDeltaTime);

        float speedRotation = 50;
        Quaternion deltaRotation = Quaternion.Euler(angleVelocity * (speedRotation * Time.fixedDeltaTime));
        _playerRigidbody.MoveRotation(_playerRigidbody.rotation * deltaRotation);
        
        _playerRigidbody.AddRelativeForce(movement, ForceMode.Impulse);
    }
    

    void ResetPosition()
    {
        _playerRigidbody.MovePosition(_startPosition);
        _playerRigidbody.MoveRotation(Quaternion.identity);
    }

    private void SpeedUp()
    {
       _playerRigidbody.AddForce(Vector3.forward * _maxSpeed,ForceMode.Impulse);
    }

}
